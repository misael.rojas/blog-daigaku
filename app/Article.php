<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Article extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $table = "articles";

    protected $fillable = ['title','content','user_id','category_id'];

    public function category ()
    {
        return $this->belongsTo('App\Category');
    }

    public function Images ()
    {
        return $this->hasMany('App\Image');
    }

    public function tags ()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }

    public function user ()
    {
        return $this->belongsTo('App\User');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
