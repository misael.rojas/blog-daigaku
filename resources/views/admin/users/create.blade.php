@extends('admin.template.main')

@section('title', 'Create user')

@section('content')

    {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Full name', 'required' ] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('name', 'Email') !!}
        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' ] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('name', 'Password') !!}
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required' ] ) !!}
    </div>

    <div class="form-group">
        {!! Form::label('name', 'Type') !!}
        {!! Form::select('type', [
        '' => '',
        'member' => 'Member',
        'Admin' => 'Admin'],
        null,
        ['class' => 'form-control', 'placeholder' => 'Select a type', 'required'
        ] ) !!}
    </div>
    </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="form-group">
                {!! Form::submit('Save', ['class' => 'btn btn-primary btn-block']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
