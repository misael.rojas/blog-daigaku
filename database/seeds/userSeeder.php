<?php

use Illuminate\Database\Seeder;
//use Illuminate\Database\Eloquent\Model;
use App\User;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $saya = new User();
        $saya->name="Eri Kamei";
        $saya->email="kamei@misaelrojas.me";
        $saya->type="admin";
        $saya->password=bcrypt("kam3i");
        $saya->save();

        factory(User::class, 10)->create();
    }
}
