<?php

use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Article::class, 50)->create();

        foreach(range(1, 50) as $index)
        {
            DB::table('article_tag')->insert([
                'article_id' => rand(1,49),
                'tag_id' => rand(1,9)
            ]);
        }

    }
}
