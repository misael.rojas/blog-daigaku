<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $type=['admin', 'member'];

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'type' => $type[rand(0,1)],
        'password' => $password ?: $password = bcrypt('kam3i'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'category' => $faker->word
    ];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'tag' => $faker->word
    ];
});

$factory->define(App\Article::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'content' => $faker->paragraphs($nb = 3, $asText = true),
        'user_id' => rand(1,5),
        'category_id' => rand(1,9),
    ];
});
